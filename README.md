# OmpTask-Lab5-SearchWord

	O problema consiste em contar quantas ocorrências de uma determinada palavra existe em um arquivo texto externo. 
 
#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
5. Compile the program using 
        gcc -fopenmp search-word.c -o search-word
6. Make a copy of the code (call it 'search-word-parallel.c' for example).
7. Parallelize the code using OmpTask (add pragmas).
8. Compare performance between serial and parallel version. Determine the speedup. Try to explain why the speedup of input 6 is less than 1. 

Anything missing? Ideas for improvements? Make a pull request.
